from selenium import webdriver
import os

print("Hello World...Python!!!")
options=webdriver.ChromeOptions()
options.add_argument("--headless")
options.add_argument("window-size=1400,1500")
options.add_argument("--disable-gpu")
options.add_argument("--no-sandbox")
options.add_argument("start-maximized")
options.add_argument("enable-automation")
options.add_argument("--disable-infobars")
options.add_argument("--disable-dev-shm-usage")
# path=os.getcwd()+"/Test/chromedriver"
# print(path)
driver = webdriver.Chrome(options=options,executable_path='/usr/bin/chromedriver')
try:
    driver.get("https://www.google.com/")
except Exception as e:
    print(e)
else:
    print("Launch browser successful")
driver.quit()
print("Quit Webdriver")